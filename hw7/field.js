let cells = document.querySelectorAll('.cell')
let step = document.getElementById('game-step')

class Player {
    constructor(){ 
        this.mode = 'x'
        this.FieldImg = 'src="./imgs/xxl-x.svg"'
        this.RoleImg = 'src="./imgs/x.svg"'
        this.userName = 'Екатерина Плюшкина'
    }

setMode (mode) {
        if (this.mode === 'x') {
            this.mode = 'o'
            this.FieldImg = 'src="./imgs/xxl-zero.svg"'
            this.RoleImg = 'src="./imgs/zero.svg"'
            this.userName = 'Владелен Пупкин'
        } else {
            this.mode = 'x'
            this.FieldImg = 'src="./imgs/xxl-x.svg"'
            this.RoleImg = 'src="./imgs/x.svg"'
            this.userName = 'Екатерина Плюшкина'
        }
}
}

currentPlayer = new Player () 
/* Начальное состояние */
step.innerHTML = `Ходит&nbsp;<img ${currentPlayer.RoleImg} />` + `&nbsp;${currentPlayer.userName}`

/* навеска обработчиков */
cells.forEach((cell) => cell.addEventListener('click', () => {
    
    /* обработка кликов по ячейкам с учетом занятости*/
    if (!cell.innerHTML) {
        cell.innerHTML = `<img ${currentPlayer.FieldImg}/>`
        currentPlayer.setMode(currentPlayer.mode)
        console.log(currentPlayer.mode)
        
        step.innerHTML = `Ходит&nbsp;<img ${currentPlayer.RoleImg} />` + `&nbsp;${currentPlayer.userName}` 
    }
}))
