import './App.css';
import React from "react"
import Header from './components/header';
import logo from './components/imgs/casual-life-3d-blue-scared-ghost 1.svg'
import Xopanel from './components/xo-panel';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {isLogin: false}
    this.handleLogin = this.handleLogin.bind(this);
  }
  handleLogin() {
    this.setState({isLogin: true});
  }
  render() {
    if (this.state.isLogin) {
      return (
        <div className="App">
          <Header></Header>
        </div>
      );
    } else {
      return (
        <div className="App">
          <Auth onLogin={this.handleLogin}></Auth>
        </div>
      );
    }
  }
}

class Auth extends React.Component {

  render() {
    // authorization = () => {
    //   const login = document.getElementsByName('login')[0].oninput = logInput;
    //   const password = document.getElementsByName('password')[0]
    //   document.querySelector('password').oninput = passInput;
    //   const logInput = () => {
    //     login.value = login.value.replace(/[^\da-z._A-Z]/g, "")
    //     };
    //   const passInput = () => {
    //         password.value = password.value.replace(/[^\da-z._A-Z]/g, "")
    //         };
    
    //     login.addEventListener("input", validateInputs);
    //     password.addEventListener("input", validateInputs);
    
     
    //     function validateInputs() {
    //         if (login.value !== "" && password.value !== "") {
    //             submitInput.style.backgroundColor = "#60C2AA";
    //         }
    //         else {
    //             submitInput.style.backgroundColor = "";
    //         }
    //     }
        
    // }
    
    return (
      <div className="body_auth">
        <div id="modal">
          <img src={logo} />
          <h1 className="head_auth">Войдите в игру</h1>
          <form id="inputs" >
            <input
              className="textfield"
              oncopy="return false;"
              id="login"
              name="login"
              placeholder="Логин"
              required
              title="Только английские буквы, цифры и знаки: . и _"
            />
            <input
              className="textfield"
              id="password"
              name="password"
              placeholder="Пароль"
              required
              title="Только английские буквы, цифры и знаки: . и _"
            />
            <input onClick={this.props.onLogin}
              type="submit"
              className="join"
              id="button_auth"
              value="Войти"
            ></input>
          </form>
          <script src="/src/components/validate.js"></script>
        </div>
      </div>
    );
  }
}

// function App(props) {
//   const isLogin = props.isLogin
//   if (isLogin) {
//     return (
//       <div className="App">
//         <Header></Header>
//       </div>
//        );

//   } else {
//     return (
    
//       <div className="App">
//         <Auth></Auth>
        
//       </div>
//        );

//   }


  
// }


export default App;
