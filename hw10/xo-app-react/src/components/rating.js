import React from "react"
import '../App.css'

function Rating() {
    return (

    <div className="body_rating">
    <div id="rating"> 
        <div id="table-name">Рейтинг игроков</div>
        <table>
            <tbody>
            <tr>
            <th className="firstcol">ФИО</th>
            <th className="stcol">Всего игр</th>
            <th className="stcol">Победы</th>
            <th className="stcol">Проигрыши</th>
            <th className="lastcol">Процент побед</th>
            </tr>
            <tr>
            <td className="firstcol">Александров Игнат Анатолиевич</td>
            <td className="stcol">24534</td>
            <td className="stcol-green">9824</td>
            <td className="stcol-red">1222</td>
            <td className="stcol">87%</td>
            </tr>
            <tr>
            <td className="firstcol">Шевченко Рафаил Михайлович</td>
            <td className="stcol">24534</td>
            <td className="stcol-green">9824</td>
            <td className="stcol-red">1222</td>
            <td className="stcol">87%</td>
            </tr>
            <tr>
            <td className="firstcol">Мазайло Трофим Артёмович</td>
            <td className="stcol">24534</td>
            <td className="stcol-green">9824</td>
            <td className="stcol-red">1222</td>
            <td className="stcol">87%</td>
            </tr>
            <tr>
            <td className="firstcol">Логинов Остин Данилович</td>
            <td className="stcol">24534</td>
            <td className="stcol-green">9824</td>
            <td className="stcol-red">1222</td>
            <td className="stcol">87%</td>
            </tr>
            <tr>
            <td className="firstcol">Борисов Йошка Васильевич</td>
            <td className="stcol">24534</td>
            <td className="stcol-green">9824</td>
            <td className="stcol-red">1222</td>
            <td className="stcol">87%</td>
            </tr>
            <tr>
            <td className="firstcol">Соловьёв Ждан Михайлович</td>
            <td className="stcol">24534</td>
            <td className="stcol-green">9824</td>
            <td className="stcol-red">1222</td>
            <td className="stcol">87%</td>
            </tr>
            <tr>
            <td className="firstcol">Негода Михаил Эдуардович</td>
            <td className="stcol">24534</td>
            <td className="stcol-green">9824</td>
            <td className="stcol-red">1222</td>
            <td className="stcol">87%</td>
            </tr>
            <tr>
            <td className="firstcol">Гордеев Шамиль Леонидович</td>
            <td className="stcol">24534</td>
            <td className="stcol-green">9824</td>
            <td className="stcol-red">1222</td>
            <td className="stcol">87%</td>
            </tr>
            <tr>
            <td className="firstcol">Многогрешный Павел Виталиевич</td>
            <td className="stcol">24534</td>
            <td className="stcol-green">9824</td>
            <td className="stcol-red">1222</td>
            <td className="stcol">87%</td>
            </tr>
            <tr>
            <td className="firstcol">Александров Игнат Анатолиевич</td>
            <td className="stcol">24534</td>
            <td className="stcol-green">9824</td>
            <td className="stcol-red">1222</td>
            <td className="stcol">87%</td>
            </tr>
            <tr>
            <td className="firstcol">Волков Эрик Алексеевич</td>
            <td className="stcol">24534</td>
            <td className="stcol-green">9824</td>
            <td className="stcol-red">1222</td>
            <td className="stcol">87%</td>
            </tr>
            <tr>
            <td className="firstcol">Кузьмин Ростислав Васильевич</td>
            <td className="stcol">24534</td>
            <td className="stcol-green">9824</td>
            <td className="stcol-red">1222</td>
            <td className="stcol">87%</td>
            </tr>
            <tr>
            <td className="firstcol">Стрелков Филипп Борисович</td>
            <td className="stcol">24534</td>
            <td className="stcol-green">9824</td>
            <td className="stcol-red">1222</td>
            <td className="stcol">87%</td>
            </tr>
            <tr>
            <td className="firstcol">Галкин Феликс Платонович</td>
            <td className="stcol">24534</td>
            <td className="stcol-green">9824</td>
            <td className="stcol-red">1222</td>
            <td className="stcol">87%</td>
            </tr>
            </tbody>
            </table>
    </div>
    </div>

    )
}

export default Rating;