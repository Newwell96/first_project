import React, { useState } from "react";
import "../App.css";
import Games from "./historydata";
import x from "./imgs/x.svg";
import o from "./imgs/zero.svg";
import trophy from "./imgs/trophy-mini.svg";


const History = (props) => {
    const [games, setPlayers] = useState(Games);

    return (
      <div id="gameslist">
        <div id="activeHeader" style={{width: '100%', height: '48px'}}>
          <h2>История игр</h2>
        </div>
        <div id="tableActive">
          <div className="gameslistheader">
            <th className="firstcolplh">Игроки</th>
            <div style={{display:'flex', gap:'16px'}}>
            <th className="stcolplh">Дата</th>
            <th className="stcolplh" style={{width: '140px'}}>Время игры</th>
            </div>
          </div>
          </div>
          {games.map((game) => (
          
          <tr className="gameslistrow" key={game.id}>
            <div className="versus">
            <td className="playercolg"><img className="role" src={o}/>{game.playerO}<img src={game.winner === 'O' ? trophy : null}/></td>
            против
            <td className="playercolg"><img className="role" src={x}/>{game.playerX}<img src={game.winner === 'X' ? trophy : null}/></td>
            </div>
            <td className="stcolg">{game.date}</td>
            <td className="stcolg" style={{width: '140px'}}>{game.duration}</td>
      
          </tr>
        ))}
        
      </div>

  );
};


export default History;