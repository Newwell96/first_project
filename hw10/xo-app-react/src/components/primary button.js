import React, {useState} from 'react';
import trophy from './imgs/trophy.svg'
import Xopanel from './xo-panel';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link
} from "react-router-dom"

const PrimaryButton = (props) => {

  if (!props.busy) {
    return (
      <div className="secondaryButton">
      {props.children}

      <button>Позвать играть</button>
    </div>
    )
    
  }
  return (
    <div className="primaryButton">
      {props.children}

      <button>Позвать играть</button>
    </div>
  );
};

export default PrimaryButton;