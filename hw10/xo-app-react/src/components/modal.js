import React from 'react';
import trophy from './imgs/trophy.svg';
import modalStore from '../store/modalStore';
import { observer } from 'mobx-react';

const Modal = observer((props) => {
  if (!modalStore.showModal) {
    return null;
  }

  const closeModal = () => {
    modalStore.closeModal();
  };

  return (
    <div className="popUp">
      {props.children}
      <img src={trophy} alt="trophy" />
      <h2>{props.winUser} победил!</h2>
      <button className="join" onClick={props.resetState} style={{ backgroundColor: '#60C2AA' }}>
        Новая игра
      </button>
      <button className="join" onClick={closeModal} style={{ backgroundColor: '#F7F7F7', color: '#373745' }}>
        Закрыть окно
      </button>
    </div>
  );
});

export default Modal;
