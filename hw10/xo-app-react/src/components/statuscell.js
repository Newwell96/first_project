import React, { useState } from 'react';

const StatusCell = (props) => {
  const [busy, setBusy] = useState(props.busy);

  const handleStatusChange = () => {
    setBusy(!busy);
    props.onStatusChange();
  };

  if (!busy) {
    return (
      <div className="statusbusy">
        {props.children}
        <button onClick={handleStatusChange}>В игре</button>
      </div>
    )
  }
  
  return (
    <div className="statusfree">
      {props.children}
      <button onClick={handleStatusChange}>Свободен</button>
    </div>
  );
};

export default StatusCell;