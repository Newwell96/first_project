import React, { useState } from 'react';
import close from './imgs/close.svg';
import male from "./imgs/male.svg";
import female from "./imgs/female.svg";

const AddPlayer = (props) => {
  const [name, setName] = useState('');
  const [age, setAge] = useState('');
  const [sex, setSex] = useState('male');
  const [showModal, setShowModal] = useState(true);



  const handleNameChange = (event) => {
    setName(event.target.value);
  };

  const handleAgeChange = (event) => {
    setAge(event.target.value);
  };

  const handleSexChange = (event) => {
    setSex(event.target.value);
  };
  
  const handleSubmit = (event) => {
    event.preventDefault();
    const newPlayer = {
      id: Date.now(),
      name,
      age,
      sex,
      blocked: false,
      create: new Date().toLocaleString(),
      changed: new Date().toLocaleString()
    };
    props.onPlayerAdd(newPlayer);
    setName('');
    setAge('');
    setSex('male');
  };

  const closeModal = () => {
    setShowModal(false);
    props.onClose();
  };

  if (!showModal) {
    return null;
  }
  return (  
    <form className='addplayermodal' onSubmit={handleSubmit}>
      <button className='rowclose' onClick={closeModal}><img src={close}/></button>
      <h2>Добавьте игрока</h2>
      <div className='inputset'>ФИО
        <input className='textfield' type="text" placeholder="Иванов Иван Иванович" value={name} onChange={handleNameChange} required />
      </div>
      <div className='rowchoose'>
        <div className='inputset' style={{width: '72px'}}> Возраст
          <input className='textfield' style={{width: '72px'}} type="number" placeholder="0" value={age} onChange={handleAgeChange} required />
        </div> 
        <div className='inputset' style={{display: 'flex'}}> Пол
        <div className='radioset'>
        <label>
            <input type="radio" value="female" checked={sex === 'female'} onChange={handleSexChange} style={{display: 'none'}} />
            <div className={sex === 'female' ? 'radiobuttonchecked' : 'radiobutton' } >
            <img src={female} alt="Female" style={{height: '32px', width:'32px'}}/>
            </div>
          </label>
          <label>
            <input type="radio" value="male" checked={sex === 'male'} onChange={handleSexChange} style={{display: 'none'}} />
            <div className={sex === 'male' ? 'radiobuttonchecked' : 'radiobutton' } >
            <img src={male} alt="Male" style={{height: '32px', width:'32px'}}/>
            </div>
          </label>
         
        </div>
        </div>
      </div>
      <button type="submit" className='join'>Добавить</button>
    </form>
  );

};

export default AddPlayer;