import React from "react"
import '../App.css'
import x from "./imgs/x.svg"
import zero from "./imgs/zero.svg"
import x_xxl from "./imgs/xxl-x.svg"
import zero_xxl from "./imgs/xxl-zero.svg"
import send_btn from "./imgs/send-btn.svg"
import Rating from "./rating"
import Modal from "./modal"
import game from "../store/gameStore"
import { observer } from "mobx-react"




function Square(props) {
    return (
      <div className="cell" onClick={props.onClick}>
        {props.value}
      </div>
    );
  }

  
//   function calculateWinner(squares) {
//     const lines = [
//       [0, 1, 2],
//       [3, 4, 5],
//       [6, 7, 8],
//       [0, 3, 6],
//       [1, 4, 7],
//       [2, 5, 8],
//       [0, 4, 8],
//       [2, 4, 6],
//     ];
//     for (let i = 0; i < lines.length; i++) {
//       const [a, b, c] = lines[i];
//       if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
//         return squares[a];
//       }
//     }
//     return null;
//   }


class Xopanel extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
          squares: Array(9).fill(null),
          xIsNext: true,
          playerImg: Array(9).fill(null),
        }
      }
      resetState = () => {
        console.log('resetStates called')
        this.setState({
            squares: Array(9).fill(null),
            xIsNext: true,
            playerImg: Array(9).fill(null),
          })
      }
      
      handleClick(i) {
        const squares = this.state.squares.slice();
        const playerImg = this.state.playerImg.slice();
        if (this.calculateWinner(squares) || squares[i]) {
          return;
        }
        squares[i] = this.state.xIsNext ? 'X': 'O';
        playerImg[i] = this.state.xIsNext ? <img src={x_xxl} /> : <img src={zero_xxl} />
        this.setState({
        squares: squares, 
        xIsNext: !this.state.xIsNext,
        playerImg: playerImg,
        })
      }
    
      renderSquare(i) {
        return (<Square value={this.state.playerImg[i]}
        onClick={() => this.handleClick(i)}
        />
        );
      }

      render() {
        console.log(game.winner)
        let status;
        if (game.winner) {
            status = <div id="game-step">Победитель: &nbsp;{(game.winner === 'X' ? <img src={x}/>: <img src={zero}/>)} &nbsp;{(this.state.xIsNext ? 'Владелен Пупкин' : 'Екатерина Плюшкина' )}</div>
            let winUser = (game.winner === 'X' ? 'Екатерина Плюшкина' : 'Владелен Пупкин' )
        return (<div id="main-container">
        {/* <div id="subject-list">
            <h1>Игроки</h1>
            <div class="container">
                <div class="subject-container">
                    <div><img class="subject-icon" src={zero} /></div>
                    <div class="subject-info">
                        <div class="subject-name">Пупкин Владелен Игоревич</div>
                        <div class="subject-percent">63% побед</div>
                    </div>
                </div>
                <div class="subject-container">
                    <div><img class="subject-icon" src={x} /></div>
                    <div class="subject-info">
                        <div class="subject-name">Плюшкина Екатерина Викторовна</div>
                        <div class="subject-percent">23% побед</div>
                    </div>
                </div>
            </div>
        </div> */}
        <div id="game-container">
            <div id="game-time">05:12</div>
            <div id="game-board">
                {this.renderSquare(0)}
                {this.renderSquare(1)}
                {this.renderSquare(2)}
                {this.renderSquare(3)}
                {this.renderSquare(4)}
                {this.renderSquare(5)}
                {this.renderSquare(6)}
                {this.renderSquare(7)}
                {this.renderSquare(8)}
                <Modal winUser = {winUser} resetState={this.resetState}/>
            </div>
            {status}
            {/* <div id="game-step">Ходит&nbsp;{(this.state.xIsNext ? <img src={x}/>: <img src={zero}/>)} &nbsp;{(this.state.xIsNext ? 'Екатерина Плюшкина':'Владелен Пупкин' )}</div> */}
        </div>
        <div id="chat-container">
            <div class="msgs-container">
                <div class="msg-container other">
                    <div class="msg-header">
                        <div class="subject-name x">Плюшкина Екатерина</div>
                        <div class="time">13:40</div>
                    </div>
                    <div class="msg-body">Ну что, готовься к поражению!!1</div>
                </div>
                <div class="msg-container me">
                    <div class="msg-header">
                        <div class="subject-name zero">Пупкин Владлен</div>
                        <div class="time">13:41</div>
                    </div>
                    <div class="msg-body">Надо было играть за крестики. Розовый — мой не самый счастливый цвет</div>
                </div>
                <div class="msg-container me">
                    <div class="msg-header">
                        <div class="subject-name zero">Пупкин Владлен</div>
                        <div class="time">13:45</div>
                    </div>
                    <div class="msg-body">Я туплю..</div>
                </div>
                <div class="msg-container other">
                    <div class="msg-header">
                        <div class="subject-name x">Плюшкина Екатерина</div>
                        <div class="time">13:47</div>
                    </div>
                    <div class="msg-body">Отойду пока кофе попить, напиши в тг как сходишь</div>
                </div>
            </div>
            <div class="msg-interactive-elements">
                <textarea placeholder="Сообщение.."></textarea>
                <button><img src={send_btn}/></button>
            </div>
        </div>
    </div>
)
        } else {
        status = <div id="game-step">Ходит&nbsp;{(this.state.xIsNext ? <img src={x}/>: <img src={zero}/>)} &nbsp;{(this.state.xIsNext ? 'Екатерина Плюшкина':'Владелен Пупкин' )}</div>;
        
        return(

            <div id="main-container">
                {/* <div id="subject-list">
                    <h1>Игроки</h1>
                    <div class="container">
                        <div class="subject-container">
                            <div><img class="subject-icon" src={zero} /></div>
                            <div class="subject-info">
                                <div class="subject-name">Пупкин Владелен Игоревич</div>
                                <div class="subject-percent">63% побед</div>
                            </div>
                        </div>
                        <div class="subject-container">
                            <div><img class="subject-icon" src={x} /></div>
                            <div class="subject-info">
                                <div class="subject-name">Плюшкина Екатерина Викторовна</div>
                                <div class="subject-percent">23% побед</div>
                            </div>
                        </div>
                    </div>
                </div> */}
                <div id="game-container">
                    <div id="game-time">05:12</div>
                    <div id="game-board">
                        {this.renderSquare(0)}
                        {this.renderSquare(1)}
                        {this.renderSquare(2)}
                        {this.renderSquare(3)}
                        {this.renderSquare(4)}
                        {this.renderSquare(5)}
                        {this.renderSquare(6)}
                        {this.renderSquare(7)}
                        {this.renderSquare(8)}
                    </div>
                    {status}
                    {/* <div id="game-step">Ходит&nbsp;{(this.state.xIsNext ? <img src={x}/>: <img src={zero}/>)} &nbsp;{(this.state.xIsNext ? 'Екатерина Плюшкина':'Владелен Пупкин' )}</div> */}
                </div>
                {/* <div id="chat-container">
                    <div class="msgs-container">
                        <div class="msg-container other">
                            <div class="msg-header">
                                <div class="subject-name x">Плюшкина Екатерина</div>
                                <div class="time">13:40</div>
                            </div>
                            <div class="msg-body">Ну что, готовься к поражению!!1</div>
                        </div>
                        <div class="msg-container me">
                            <div class="msg-header">
                                <div class="subject-name zero">Пупкин Владлен</div>
                                <div class="time">13:41</div>
                            </div>
                            <div class="msg-body">Надо было играть за крестики. Розовый — мой не самый счастливый цвет</div>
                        </div>
                        <div class="msg-container me">
                            <div class="msg-header">
                                <div class="subject-name zero">Пупкин Владлен</div>
                                <div class="time">13:45</div>
                            </div>
                            <div class="msg-body">Я туплю..</div>
                        </div>
                        <div class="msg-container other">
                            <div class="msg-header">
                                <div class="subject-name x">Плюшкина Екатерина</div>
                                <div class="time">13:47</div>
                            </div>
                            <div class="msg-body">Отойду пока кофе попить, напиши в тг как сходишь</div>
                        </div>
                    </div>
                    <div class="msg-interactive-elements">
                        <textarea placeholder="Сообщение.."></textarea>
                        <button><img src={send_btn}/></button>
                    </div>
                </div> */}
            </div>
            )
    }
    
    
}
}

export default observer(Xopanel);