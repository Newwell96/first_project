import { makeObservable, observable, action } from 'mobx';

class ModalStore {
  showModal = true;

  constructor() {
    makeObservable(this, {
      showModal: observable,
      closeModal: action,
    });
  }

  closeModal() {
    this.showModal = false;
  }
}

const modalStore = new ModalStore();
export default modalStore;
