const gameField = [
    ['x', 'o', null],
    ['x', null, 'o'],
    ['x', 'o', 'o']
    ];
let winnerSymbol = '';
let flag = false; //объявляем глобальную переменную-флаг, сигнализирующую о наличии победителя

//сначала проверяем по строкам    
for (let r = 0; r <= 2; r++) {
    if (gameField[r][0] == gameField[r][1] && gameField[r][1] == gameField[r][2] && gameField[r][0] != null) {
        winnerSymbol = gameField[r][0];
        flag = true;
        break
    } else if (r <= 2){
        continue
    }
}

//если совпадений нет - проверяем по столбцам
if (flag === false) {
    for (let c = 0; c <= 2; c++) {
        
        if (gameField[0][c] == gameField[1][c] && gameField[1][c] == gameField[2][c] && gameField[0][c] != null) {
            flag = true;
            console.log(flag)
            winnerSymbol = gameField[0][c];
            break;
        } else if (c <= 2){
            continue
        }
    }
}

//если совпадений по прежнему нет, проверяем диагонали
if (flag === false) {
    let startValue = gameField[0][0];
    let counter = 0;
    for (let r = 0; r <= 2; r++) {
        for (let c = 0; c <= 2; c++){ //главная диагональ
            if (r == c && gameField[r][c] == startValue && gameField[r][c] != null) {
                counter++
            }
        }
    }
    if (counter == 3) {
        flag = true;
        winnerSymbol = startValue;
    }
}

if (flag === false) {
    console.log('побочная диагональ')
    let startValue = gameField[2][0];
    let counter = 0;
    for (let r = 0; r <= 2; r++) {
        for (let c = 0; c <= 2; c++){ //побочная диагональ
            if (r == (3 - c - 1) && gameField[r][c] == startValue && gameField[r][c] != null) {
                counter++
            }
        }
    }
    if (counter == 3) {
        flag = true;
        winnerSymbol = startValue;
    }
    }
// если совпадений нет ни в одном из условий, проверяем закончена ли игра
if (flag === false){
    for (let r = 0; r <= 2; r++) {
        if (gameField[r].includes(null)){ //если есть null (незаполненное поле), то игра еще не закончена
        alert('Игра ещё идёт и пока победителя нет')
        break
        } else if (r = 2){ //если совпадений нет ни в одном условии, а также нет null (игра завершена) - значит ничья
        alert('Ничья')
        } else { 
        continue 
        }
    }

    
}
if (flag === true) {
    if (winnerSymbol === 'x') {
        alert('Победили крестики!')
    } else if (winnerSymbol === 'o'){
        alert('Победили нолики!')
    } else {
        alert('О_о. вы сломали систему или поле больше чем 3х3')
    }
}
        //console.log(gameField)

