import React from "react"
import '../App.css'
import MySwitchToggle from "./switch_toogle"
import PrimaryButton from "./primary button";
import StatusCell from "./statuscell";
import ActiveRow from "./active_row";


class Active extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            checked: false,
        }
    }
    
    render() {
        const { checked } = this.state;

        console.log(this.state.checked)
return (

    <body>   
    <div id="activePlayers"> 
    
    <div id="activeHeader"><h2>Активные игроки</h2>
        <div className="switchActive">Только свободные 
        <MySwitchToggle checked={this.state.checked} onchange={(value) => this.setState({checked: value})}/>
        </div>
    </div> 
    
    <div id="tableActive">
        <ActiveRow player='Александров Игнат Анатолиевич' show={!checked}/>
        <ActiveRow player='Василенко Эрик Платонович' show={!checked}/>
        <ActiveRow player='Быков Юрий Виталиевич' show={!checked}/>
        <ActiveRow player='Галкин Феликс Платонович' show={!checked}/>
        <ActiveRow player='Комаров Цефас Александрович' show={!checked}/>
        <ActiveRow player='Шевченко Рафаил Михайлович' show={!checked}/>
        <ActiveRow player='Гордеев Шамиль Леонидович' show={!checked}/>
        <ActiveRow player='Бобров Фёдор Викторович' show={!checked}/>
        <ActiveRow player='Суворов Феликс Григорьевич' show={!checked}/>
        <ActiveRow player='Марков Йошка Фёдорович' show={!checked}/>
    </div>
    </div>
    
    </body>

)

} }

export default Active;