import React from "react"
import '../App.css'
import Active from "./active"
import logo from "./imgs/s-logo.svg"
import signout from "./imgs/signout-icon.svg"
import {
    BrowserRouter as Router,
    Routes,
    Route,
    Link
} from "react-router-dom"
import Xopanel from "./xo-panel"
import Rating from "./rating"
import PlayersList from "./playerslist"
import History from "./history"


function Header() {
    const [state, setState] = React.useState({ activeItem: 0 });
    const handleClick = (event, { index }) => setState({ activeItem: index });

    return (
        <Router>
        <header>
            <div id="logo"><img src={logo} /></div>
            <ul>
            <div id="nav-panel">
                <li><Link to="/xo-panel"><div className={state.activeItem === 0 ? 'active' : ''} onClick={(event) => handleClick(event, { index: 0 })}> Игровое поле</div></Link></li>
                <li><Link to="/rating"><div className={state.activeItem === 1 ? 'active' : ''} onClick={(event) => handleClick(event, { index: 1 })}>Рейтинг</div></Link></li>
                <li><Link to="/active"><div className={state.activeItem === 2 ? 'active' : ''} onClick={(event) => handleClick(event, { index: 2 })}>Активные игроки</div></Link></li>
                <li><Link to="/history"><div className={state.activeItem === 3 ? 'active' : ''} onClick={(event) => handleClick(event, { index: 3 })}>История игр</div></Link></li>
                <li><Link to="/playerslist"><div className={state.activeItem === 4 ? 'active' : ''} onClick={(event) => handleClick(event, { index: 4 })}>Список игроков</div></Link></li>
            </div>
            </ul>
            <button><img src={signout}/></button>
        </header>

            <Routes>
            <Route path="/header" element={<Header/>}></Route>
            <Route path="/xo-panel" element={<Xopanel/>}></Route>
            <Route path="/rating" element={<Rating/>}></Route>
            <Route path="/" element={<Xopanel/>}></Route>
            <Route path="/active" element={<Active/>}></Route>
            <Route path="/playerslist" element={<PlayersList/>}></Route>
            <Route path="/history" element={<History/>}></Route>
        </Routes>
        </Router>
    );
    }

export default Header;



// function Header() {
//     return (
//         <header>
//             <div id="logo"><img src={logo} /></div>
//             <div id="nav-panel">
//                 <div>Игровое поле</div>
//                 <div className="active">Рейтинг</div>
//                 <div>Активные игроки</div>
//                 <div>История игр</div>
//                 <div>Список игроков</div>
//             </div>
//             <button><img src={signout} /></button>
//         </header>

//     );
//     }
