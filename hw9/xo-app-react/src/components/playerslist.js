
import React, { useState } from "react";
import "../App.css";
import Players from "./players";
import male from "./imgs/male.svg";
import female from "./imgs/female.svg";
import BlockStatus from "./blockstatus";
import BlockButton from "./blockbutton";
import AddPlayer from "./addplayer";


const PlayersList = (props) => {
  const [players, setPlayers] = useState(Players);
  const [showAddPlayer, setShowAddPlayer] = useState(false);

  const handleBlockStatusChange = (playerId) => {
    const updatedPlayers = players.map((player) =>
      player.id === playerId ? { ...player, blocked: !player.blocked } : player
    );
    setPlayers(updatedPlayers);
  };

  const handlePlayerAdd = (newPlayer) => {
    setPlayers([...players, newPlayer]);
    setShowAddPlayer(false);
  };

  const handleShowAddPlayer = () => {
    setShowAddPlayer(!showAddPlayer);
  };

  const handleCloseAddPlayer = () => {
    setShowAddPlayer(false);
  };

  return (
    <div id="playerslist">
      <div id="activeHeader" style={{width: '100%', height: '48px'}}>
        <h2>Список игроков</h2>
        {showAddPlayer ? <AddPlayer onClose={handleCloseAddPlayer} onPlayerAdd={handlePlayerAdd}/> : null}
        <button className='primaryButton' style={{height: '48px', width:'173px'}} onClick={handleShowAddPlayer}>Добавить игрока</button>
      </div>
      <div id="tableActive">
        <div className="playerslistheader">
          <th className="firstcolplh">ФИО</th>
          <th className="stcolplh" style={{ width: "108px" }}>
            Возраст
          </th>
          <th className="minicolh">Пол</th>
          <th className="stcolplh" style={{ width: "152px" }}>
            Статус
          </th>
          <th className="stcolplh">Создан</th>
          <th className="stcolplh">Изменен</th>
          <th className="lastcolplh"></th>
        </div>
        {players.map((player) => (
          
          <div className="playerslistrow" key={player.id}>
            <td className="firstcolpl">{player.name}</td>
            <td className="stcolpl" style={{ width: "108px" }}>
              {player.age}
            </td>
            <td className="minicol">
              {player.sex === "male" ? <img src={male} /> : <img src={female} />}
            </td>
            <td className="stcolpl">
              <BlockStatus blocked={player.blocked} />
            </td>
            <td className="stcolpl">{player.create}</td>
            <td className="stcolpl">{player.changed}</td>
            <BlockButton
              blocked={player.blocked}
              onStatusChange={() => handleBlockStatusChange(player.id)}
            />
          </div>
        ))}
        
      </div>
    </div>
  );
};

export default PlayersList;
