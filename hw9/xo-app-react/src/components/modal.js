import React, {useState} from 'react';
import trophy from './imgs/trophy.svg'
import Xopanel from './xo-panel';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link
} from "react-router-dom"

const Modal = (props) => {
  const [showModal, setShowModal] = useState(true);

  const closeModal = () => {
    setShowModal(false);
  };

  if (!showModal) {
    return null;
  }
  return (
    <div className="popUp">
      {props.children}
      <img src={trophy}/>
      <h2>{props.winUser} победил!</h2>
      <button className='join' onClick={props.resetState} style={{backgroundColor: '#60C2AA'}}>Новая игра</button>
      <button className='join' onClick={closeModal} style={{backgroundColor: '#F7F7F7', color: '#373745'}}>Закрыть окно</button>
    </div>
  );
};

export default Modal;