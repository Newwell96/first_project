import React from 'react';

const BlockStatus = (props) => {
  if (!props.blocked) {
    return (
      <div className="statusblocked">
        <button>Заблокирован</button>
      </div>
    )
  }
  
  return (
    <div className="statusactive">
      <button>Активен</button>
    </div>
  );
};

export default BlockStatus;