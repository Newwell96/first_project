import React, {useState} from 'react';
import styled from 'styled-components';
import PropTypes from "prop-types";

const Container = styled.label`
    position: relative;
    display: inline-block;
    width: 33px;
    height: 14px;


    > input {
        display: none;
}
`;

const Slider = styled.div`
position: absolute;
display: flex;
align-items: center;
cursor: pointer;
top: 0;
left: 0;
right:0;
bottom:0;
background-color: #DCDCDF;
transition: 0.4s;
border-radius: 15px;
  
&:before {
    position: absolute;
    content: "";
    height: 20px;
    width: 20px;
    align-content: center;
    background-color: #F7F7F7;
    transition: 0.2s;
    border-radius:50%;
}
`;

const SliderInput = styled.input`
    &:checked + ${Slider} {
    background-color: #60C2AA;
      //mix-blend-mode: normal;
      //opacity: 0.5;
      
    &:before {
        transform: translateX(15px);
        background-color: #3BA189;

}
}
`;

class MySwitchToggle extends React.Component {

    render() {
        return (
            <Container>
                <SliderInput
                    type="checkbox"
                    checked={this.props.checked}
                    onChange ={
                    (e) => this.props.onchange(e.target.checked)
                    }
                />

                <Slider/>
            </Container>
        );
    }
}

MySwitchToggle.propTypes = {
    checked: PropTypes.bool,
    onchange: PropTypes.func,
}

export default MySwitchToggle;