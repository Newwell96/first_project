import React, { useState } from 'react';
import block from "./imgs/block.svg";

const BlockButton = (props) => {
    const [blocked, setBlocked] = useState(props.blocked);
    const handleStatusChange = () => {
      setBlocked(!blocked);
      props.onStatusChange();
    };
  
    if (!blocked) {
      return (
        <div className="unblock">
          <button onClick={handleStatusChange}>Разблокировать</button>
        </div>
      )
    }
    
    return (
      <div className="block">
        <button className='blockbutton' onClick={handleStatusChange}><img className='blockimg' src={block} /> Заблокировать</button>
      </div>
    );
  };

  export default BlockButton;