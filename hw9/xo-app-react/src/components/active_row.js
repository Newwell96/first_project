import React, {useState} from 'react';
import PrimaryButton from './primary button';
import StatusCell from './statuscell';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link
} from "react-router-dom"

const ActiveRow = (props) => {
  const [busy, setBusy] = useState(true);

  const handleStatusChange = () => {
    setBusy(!busy);
  };
  
   if (!props.show && !busy) {
    return (
     null
    )
  } 
  return (
    <div className="activerow">
      <div className='activePlayerName'>{props.player}</div>
      <div className='right'><StatusCell busy={busy} onStatusChange={handleStatusChange}/>
      <PrimaryButton busy={busy} call={busy}/></div>
      
       
    </div>
  );
};

export default ActiveRow;