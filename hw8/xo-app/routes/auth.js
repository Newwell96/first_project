var alert = require('alert')
var express = require('express');
const authorize = require("../modules/auth_checker");
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('auth',{hasError: req.query.error});
});

router.post('/', async function(req, res, next) {
  console.log('вы в auth.js')
  const authData = req.body;
  const login = authData.login;
  const password = authData.password;

  const sessionID = await authorize(login, password);
  if (sessionID === '') {
    res.redirect("/auth?error=true")
    return
  }
  res.cookie("sessionID", sessionID)
  console.log(sessionID)
  res.redirect("/rating")
  
});

module.exports = router;
