var express = require('express');
var router = express.Router();
const authMiddleware = require("../middleware/auth")


/* GET users listing. */
router.get('/', function(req, res, next) {
  var players = [
    { name: 'Александров Игнат Анатолиевич', games: 100, wins: 70, loses: 30, winrait: 70},
    { name: 'Шевченко Рафаил Михайлович', games: 100, wins: 70, loses: 30, winrait: 70},
    { name: 'Мазайло Трофим Артёмович', games: 100, wins: 70, loses: 30, winrait: 70},
    { name: 'Логинов Остин Данилович', games: 100, wins: 70, loses: 30, winrait: 70},
    { name: 'Борисов Йошка Васильевич', games: 100, wins: 70, loses: 30, winrait: 70},
    { name: 'Соловьёв Ждан Михайлович', games: 100, wins: 70, loses: 30, winrait: 70},
    { name: 'Негода Михаил Эдуардович', games: 100, wins: 70, loses: 30, winrait: 70},
    { name: 'Гордеев Шамиль Леонидович', games: 100, wins: 70, loses: 30, winrait: 70},
    { name: 'Многогрешный Павел Виталиевич', games: 100, wins: 70, loses: 30, winrait: 70},
    { name: 'Александров Игнат Анатолиевич', games: 100, wins: 70, loses: 30, winrait: 70},
    { name: 'Волков Эрик Алексеевич', games: 100, wins: 70, loses: 30, winrait: 70},
    { name: 'Кузьмин Ростислав Васильевич', games: 100, wins: 70, loses: 30, winrait: 70},
    { name: 'Стрелков Филипп Борисович', games: 100, wins: 70, loses: 30, winrait: 70},
    { name: 'Галкин Феликс Платонович', games: 100, wins: 70, loses: 30, winrait: 70},
  ];
  res.render('rating', {players});
});

module.exports = router;
