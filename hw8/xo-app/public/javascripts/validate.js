const submitInput = document.getElementById("button_auth");
const logInput = () => {
    login = document.getElementsByName('login')[0]
    login.value = login.value.replace(/[^\da-z._A-Z]/g, "")
    };
const passInput = () => {
        password = document.getElementsByName('password')[0]
        password.value = password.value.replace(/[^\da-z._A-Z]/g, "")
        };

    document.querySelector('input[name=login]').oninput = logInput;
    document.querySelector('input[name=password]').oninput = passInput;


    login.addEventListener("input", validateInputs);
    password.addEventListener("input", validateInputs);

 
    function validateInputs() {
        if (login.value !== "" && password.value !== "") {
            submitInput.style.backgroundColor = "#60C2AA";
        }
        else {
            submitInput.style.backgroundColor = "";
        }
    }
    