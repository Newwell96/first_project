const authMiddleware = function (req, res, next) {
    if (!req.cookies.sessionID) {
        res.redirect("/auth")
        return
    }
    next()
}

module.exports = authMiddleware;