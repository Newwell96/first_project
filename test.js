const crypto = require('bcrypt')
const salt = 10
const hashpass = '$2b$10$4EQBjGfSjQz8INLOqCqyqe2TciE.jdIJ4HCT0H0/fcFuUCb5ur4/u'

let password = 'admin'
crypto.hash(password, salt, function(err, hash) {
    console.log(hash)
    crypto.compare(password, hashpass, function(err, result) {
        if (err) {
            console.error(err)
        } else {
            console.log(result)
        }
    })
    
})

